#!/bin/bash

if [ `whoami` != 'root' ];then
    echo "Ce programme nécessite d'être en root. Essayer avec sudo."
    exit 1
fi
echo "# Paquets utiles et inutiles"
apt-get -y install vim numlockx synaptic vlc gedit xsane default-jre flashplugin-installer icedtea-plugin emacs perl-tk clamav clamtk
#... flashplugin-nonfree
echo "Pour Clamav, noubliez pas de configurer le proxy dans /etc/clamav/freshclam.cfg :"
echo "HTTPProxyServer monproxy.com"
echo "HTTPProxyPort 3128"
apt-get -y remove abiword gnumeric transmission-gtk transmission-common
echo "## Désactivation du bluetooth"
update-rc.d -f bluetooth remove

echo "# Paquets pédagogiques"
apt-get -y install libreoffice geogebra geogebra-gnome freeplane dia stellarium scite
#... gromit

echo "# Giac - Xcas"
#http://www-fourier.ujf-grenoble.fr/~parisse/install_fr.html#packages
sources=/etc/apt/sources.list
if [ `grep -c http://www-fourier.ujf-grenoble.fr/ $sources` -lt 1 ];then
    echo "deb http://www-fourier.ujf-grenoble.fr/~parisse/debian/ stable main " >> $sources
fi
clepgp=xcas_public_key.gpg
wget http://www-fourier.ujf-grenoble.fr/~parisse/$clepgp
apt-key add $clepgp
rm -f $clepgp
apt-get update
apt-get -y install giac qcas 

echo "# OCS inventory (agent)"
apt-get -y install ocsinventory-agent dmidecode libxml-simple-perl libnet-ip-perl libwww-perl libdigest-md5-perl libnet-ssleay-perl
echo "server=http://ocsinventory-ng/ocsinventory" > /etc/ocsinventory/ocsinventory-agent.cfg

echo "# Péparation du système d'impression"
# Pré-installation des drivers d'imprimante
apt-get -y install cups cups-pdf printer-driver-all brother-cups-wrapper-laser brother-cups-wrapper-laser1
# Désactivation de la découverte auto des imprimantes
sed -i -e 's/^Browsing On/Browsing Off/' /etc/cups/cupsd.conf

echo "# Émulation windows : POL, Office 2010..."
apt-get -y install curl p7zip-full playonlinux mono-complete ttf-mscorefonts-installer
read -n 1 -s -p '## Veuillez installer Office 2010 avec POL, puis appuyez sur une touche ##'
# Dossier où sera copié POL
copie_POL=/var/POL
# Dossier contenant les raccourcis bureau
desktop=$copie_POL/shortcuts
# Le script à éxecuter à l'ouverture de session en sudo
script_sudo=/usr/local/bin/office.sh

echo ""
echo "## Copie du dossier POL... (peut prendre quelques minutes)"
rm -rf $copie_POL
cp -R ~/.PlayOnLinux $copie_POL
# Paramétrage de sudo pour notre script uniquement
sed -i -e 's/^root ALL=(ALL) ALL/#&/' /etc/sudoers
echo "ALL ALL=NOPASSWD: $script_sudo" > /etc/sudoers.d/office	
# Création du script sudo
cat > $script_sudo << EOF
#!/bin/bash
# Attribution du dossier POL
# \$1 : username
groupe=\$(id -gn \$1)
uid=\$(id -u \$1)
POL=$copie_POL
home=\$(eval echo ~\$1)
lien=\$home/.PlayOnLinux

# Sauvegarde du lien précédent
if [ -d "\$lien" ]; then
    mv \$lien \$lien.bak
fi
# Spécifique à Office 2010 : création du dossier utilisateur ...
users=\$POL/wineprefix/Office2010/drive_c/users/\$uid
mkdir \$users
# ... et des liens symboliques pour que office puisse retrouver 
# les dossiers "Mes documents" et "Bureau"
if [ ! -h \$users/Mes\ documents ];then
	ln -s \$home \$users/Mes\ documents
fi
if [ ! -h \$users/Bureau ];then
	ln -s \$home/Bureau/ \$users/Bureau
fi
# Changement de propriétaire du dossier de POL
chown -h -R \$1:\$groupe \$POL
ln -s \$POL \$lien
chown -h -R \$1:\$groupe \$lien
# Copie des raccourcis sur le bureau
cp --preserve=ownership \$POL/shortcuts/*.desktop \$home/Bureau
EOF
chmod +x-w $script_sudo

echo "## Paramétrage du lancement automatique au login (graphique uniquement)"
$office=/etc/xprofile
echo "#!/bin/sh" > $office
echo "bash -c \"sudo $script_sudo \"$(whoami) > ~/.office.log 2>&1" >> $office
chmod +x-w $office

echo "## Paramétrage des raccourcis des applications"
for app in Word Excel Powerpoint
do
    # Modification du shortcut de POL
    sed -i -e 's/export WINEPREFIX=.*/export WINEPREFIX="$(eval echo ~$USER)\/.PlayOnLinux\/wineprefix\/Office2010/"' "$copie_POL/shortcuts/Microsoft $app 2010"
    sed -i -e 's/cd \"\/home.*/cd \"$WINEPREFIX\/drive_c\/Program Files\/Microsoft Office\/Office14\"/' "$copie_POL/shortcuts/Microsoft $app 2010"
    # Création du raccourci bureau
    cat > "$desktop/$app 2010.desktop" << EOF
[Desktop Entry]
Name=$app 2010
Comment=PlayOnLinux
Type=Application
Exec=/usr/share/playonlinux/playonlinux --run "Microsoft $app 2010" %F
Icon=$copie_POL/icones/full_size/Microsoft $app 2010
Name[fr_FR]=$app 2010
StartupWMClass=$app 2010.exe
Categories=
EOF
	chmod +x "$desktop/$app 2010.desktop"
done

