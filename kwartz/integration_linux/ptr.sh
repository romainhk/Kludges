#!/bin/bash
# Installation automatique d'imprimantes
#
# Les affectations se font à partir d'un fichier ini présent sur un partage smb
#
# == Installation ==
# * Mettre le script dans /usr/local/bin/
# * Ajouter dans /etc/rc.local :
#while [ `status cups|grep "start/running"|wc -l` = "0" -o `ip route|wc -l` = "0" ] ; do
#    # On attend que cups et le réseau soient disponible
#    sleep 3
#done && bash -c "sudo /usr/local/bin/ptr.sh &> /tmp/ptr.log"

#
# * ptr.sh nécessite les droits administrateurs (à cause de lpadmin) ;
# au cas où le compte n'est pas sudoer,
# il faut aussi ajouter une déclaration dans /etc/sudoers.d/ :
#  ALL ALL=NOPASSWD: /usr/local/bin/ptr.sh
# * Reste plus qu'à protéger le fichier un minimum :
# > chmod 111 /usr/local/bin/ptr.sh
#
# == Fichier de configuration ==
# Le fichier de configuration doit être structuré comme un fichier ini :
# [NOM_DE_L_IMPRIMANTE.GROUPE_D_AFFECTATION]
# affectations=LISTE_DE_NOMS_OU_DE_GROUPES_DE_POSTES
# ppd=SI_BESOIN,_PRECISEZ_ICI_UN_NOM_DE_DRIVER
#
# Exemple :
# [ptr-204.Salle204]
# affectations=Salle204, prof-202, video-203
# ppd=postscript-hp:0/ppd/hplip/HP/hp-laserjet_2300-ps.ppd
#
# L'imprimante HP 2300 ptr-204 sera affectée aux postes de la salle 204,
# au poste prof de la salle 202 et au poste de projection de la salle 203
#
# Pour rechercher un driver présent sur un l'ordinateur :
# > lpinfo --make-and-model 'Xerox' -m
# Pour installer un driver en ppd, il suffit de le déposer dans le dossier /usr/share/ppd

#================================================
# Options à personnaliser
smbpartage=//kwartz-server/netlogon
smbuser=XXX
smbpass=XXX
smbini=CONFIG.INI

#================================================
# Détermination automatique des autres paramètres
poste=`hostname -A|cut -d'.' -f1`
ip=`hostname -I`
# Détermination du domaine (qui devrait être donnée par dnsdomainename :/)
fqdn=`host $ip |grep --only-matching --perl-regex "(?<=domain name pointer ).*"`
echo "fqdn#"$fqdn
domaine=`grep "search" /etc/resolv.conf | cut -d' ' -f2`
groupe=${fqdn%"."}
groupe=${groupe#"$poste"}
groupe=${groupe#"."}
groupe=${groupe%"$domaine"}
groupe=${groupe%"."}
echo "groupe#$groupe"

if [ -z "$poste" -o -z "$groupe" ];then
	echo "! Pas de nom de groupe ou de poste -> pas d'imprimantes"
	exit 2
fi

# Lecture des affectations
fichierconf=/tmp/config.cfg
tmp=/tmp/install_ptr
smbclient $smbpartage -U$smbuser%$smbpass -c "get $smbini $fichierconf"
tr -d '\015' < $fichierconf > $tmp
mv $tmp $fichierconf
# Retrait des commentaires
sed '/^#/ d' < $fichierconf > $tmp
mv $tmp $fichierconf

# Parsing du fichier ini
# http://serverfault.com/questions/345665/parse-and-convert-ini-into-bash-array-variables
declare -A affectations
declare -A ppd
declare -a sections # la liste des imprimantes connues
while IFS='= ' read var val
do
    if [[ $var == \[*] ]]
    then
        section=${var//[.-]/}
		# En retirant les . et les -
		sections=("${sections[@]}" ${var//[\[\]]/})
		# On garde les noms de section sans les crochets
    elif [[ $val ]]
    then
        declare "$var$section=$val"
    fi
done < $fichierconf

# Ménage : Pour chaque imprimante déjà installée...
liste=`lpstat -p`
for ptr in `echo $liste|grep --perl-regexp --only-matching "printer [^ ]+ "|cut -d' ' -f2`
do
	if [[ $ptr != "PDF" ]];then # On conserve l'imprimante PDF
		echo "Suppression de $ptr"
		lpadmin -x $ptr
	fi
done

# Pour la suite, insensibilité à la casse
shopt -s nocasematch

# Installation des imprimantes affectées
for i in "${!sections[@]}"
do
	sec=${sections[$i]}
	aff=${affectations[${sec//[.-]/}]}
	if [[ $aff == *$groupe* ]] || [[ $aff == *$poste* ]];then
		# On cherche l'ip de l'imprimante en question
		ipptr=`host $sec.$domaine|grep --only-matching --perl-regex "(?<=has address ).*"`
		drv=${ppd[${sec//[.-]/}]}
		if [[ $drv ]];then # si un driver est défini
			echo "Installation de $sec ($ipptr / $drv)"
			lpadmin -p $sec -E -v socket://$ipptr -m $drv -o printer-is-shared=false
		else
			echo "Installation de $sec ($ipptr)"
			lpadmin -p $sec -E -v socket://$ipptr -o printer-is-shared=false
		fi
		# On met par défaut la dernière imprimante installé (et donc, pas l'imprimante PDF)
		lpadmin -d $sec
	fi
done

# Nettoyage
rm -f $fichierconf
rm -f $tmp
