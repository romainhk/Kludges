#!/bin/bash
# Script d'intégration d'Ubuntu 13.10 à un réseau Kwartz
# - Nécessite une connexion internet et d'être en root
# - Dans Kwartz~Control, mettre l'ordi en "Authorisé non filtré" le temps de l’installation

if [ `whoami` != 'root' ];then
    echo "Ce programme nécessite d'être en root. Essayer avec sudo."
    exit 1
fi
echo "-----------------------------------"
echo "Récupération automatique du proxy..."
ip=`ip route | grep "default via" | cut -d" " -f3`
if [ -z $ip ];then
    echo "Impossible de retrouver l'ip du serveur Kwartz"
    read -p "Veuiller la saisir : " ip
else
# Test la connexion internet ici !!
    echo "L'adresse de votre proxy kwartz est $ip"
fi
echo "-----------------------------------"
# Disclaimer
echo "NB: Si vous voulez pouvoir construire une image sur kwartz, il faut que Linux soit installé sur une unique partition ext3 dont les inodes font 128 bits (Rembo5/Tivoli)."
echo "Voir http://www.kwartz.com/Installation-d-un-poste-Ubuntu-11.html pour l'utilisation manuelle de mkfs.ext3"
echo "De plus, il est indispensable d'utiliser lilo comme bootloader, et non grub"
read -n 1 -s -p '... Appuyez sur une touche pour continuer ...'
# Proxy
echo ""
echo "##  Déclaration du proxy  ##"
proxy=/etc/environment
#touch $proxy # car il n'existe certainement pas
if [ `grep -c tp_proxy $proxy` -lt 1 ];then
    echo "# Configuration du proxy (console)" >> $proxy
    echo "export http_proxy=http://$ip:3128" >> $proxy
    echo "export https_proxy=https://$ip:3128" >> $proxy
    echo "export ftp_proxy=ftp://$ip:3128" >> $proxy
fi
chmod +x $proxy
source $proxy
echo "## ... et dans les sessions X"
xproxy=/etc/profile.d/xproxy.sh
cat > $xproxy << EOF
# Configuration du proxy (mode graphique)
if [ -n "\$DISPLAY" ];then
  gsettings set org.gnome.system.proxy mode 'manual'
  for proto in http https ftp
  do
    gsettings set org.gnome.system.proxy.\$proto host '$ip'
    gsettings set org.gnome.system.proxy.\$proto port 3128
  done
fi
EOF
$xproxy
echo "## ... et pour apt"
aptproxy=/etc/apt/apt.conf.d/proxy
echo "Acquire::http::Proxy \"http://$ip:3128\";" > $aptproxy
echo "Acquire::ftp::Proxy \"ftp://$ip:3128\";" >> $aptproxy

# Mises à jour
echo "##  Mises à jour  ##"
apt-get update
apt-get -y dist-upgrade
echo "## Installation des paquets localisés en français"
apt-get -y install language-pack-fr language-pack-fr-base firefox-locale-fr libreoffice-l10n-fr libreoffice-help-fr thunderbird-locale-fr hunspell-fr hyphen-fr mythes-fr wfrench
# Paquets requis pour un support complet des langues (évite un message d'alerte)
apt-get -y install mythes-en-au myspell-en-za gimp-help-fr thunderbird-locale-en-us openoffice.org-hyphenation aspell-fr mythes-en-us libreoffice-help-en-gb libreoffice-help-en-us libreoffice-l10n-en-gb libreoffice-l10n-en-za myspell-en-gb thunderbird-locale-en myspell-en-au gimp-help-en hyphen-en-us wbritish thunderbird-locale-en-gb hunspell-en-ca

echo "## Installation de lilo"
read -p "Procéder à l'installation de lilo ? [O/n] : " choix
case $choix in
    [nN])
    ;;
    *)
    apt-get -y install lilo
    liloconfig -f
    # Retrait de l'écran de sélection du kernel
    sed -i -e 's/^prompt/#&/' /etc/lilo.conf
    # Définition du disque de démarrage
    sed -i -e 's/^boot = \/dev\/disk/#&/' /etc/lilo.conf
    sed -i -e 's/^#boot = \/dev\/sda/boot = \/dev\/sda/' /etc/lilo.conf
    sed -i -e 's/root = "UUID/#&/' /etc/lilo.conf
    sed -i -e 's/#root = \/dev/root = \/dev/' /etc/lilo.conf
    lilo;;
esac

echo "## Coupure des màj auto"
sed -i -e 's/APT::Periodic::Update-Package-Lists "1"/APT::Periodic::Update-Package-Lists "0"/' /etc/apt/apt.conf.d/10periodic
echo "## Coupure du crash report (apport)"
sed -i -e "s/enabled=1/enabled=0/" /etc/default/apport
echo "## Configuration de l'horloge (NTP)" # Kwartz peut faire office de serveur de temps
sed -i -e 's/NTPDATE_USE_NTP_CONF=yes/NTPDATE_USE_NTP_CONF=no/' /etc/default/ntpdate
sed -i -e "s/NTPSERVERS=\"ntp.ubuntu.com\"/NTPSERVERS=\"$ip\"/" /etc/default/ntpdate
ntpdate-debian

echo "## Modification du dhcp pour qu'il récupère le bon hostname"
dhclient=/etc/dhcp/dhclient.conf
sed -i -e 's/^send host-name = gethostname();/#&/' $dhclient

# Authentification LDAP
echo "------------------------------------"
echo "## Authentification des utilisateurs sur le réseau et 'liaison au domaine' ##"
echo "Les réponses à donner, dans l'ordre : "
echo "* ldap://$ip"
echo "* dc=monlycee,dc=fr (remplacer par votre nom de domaine)"
echo "* 3, non, non"
read -n 1 -s -p '... Appuyez sur une touche pour continuer ...'
apt-get -y install openbsd-inetd pidentd libpam-ldap
sed -i -e 's/^pam_password md5/#&/' /etc/ldap.conf
sed -i -e 's/#pam_password crypt/pam_password crypt/' /etc/ldap.conf
echo "## Modification de la configuration pam pour ldap"
auth-client-config -t nss -p lac_ldap

echo "## Ajout des lecteurs réseaux"
apt-get -y install libpam-mount cifs-utils
pammount=/etc/security/pam_mount.conf.xml
for lecteur in homes commun public
do
  nomm=( $lecteur )
  nom="${nomm[@]^}" # <=> Capitalize
  if [ "$nom" = "Homes" ];then
      nom="LecteurH"
  fi
  if [ `grep -c mountpoint=\"~/$lecteur $pammount` -lt 1 ];then
      sed -i -e "/<\/pam_mount>/i\<volume uid=\"1000-65532\" fstype=\"cifs\" server=\"$ip\" path=\"$lecteur\" mountpoint=\"~/$nom\" options=\"iocharset=utf8\" />" $pammount
  fi
done
echo "## Modification de /etc/pam.d/common-session"
pamcommon=/etc/pam.d/common-session
if [ `grep -c "pam_mkhomedir.so umask=077 silent skel=/etc/skel/" $pamcommon` -lt 1 ];then
    sed -i -e '/session.*optional.*mount/i\session required\tpam_mkhomedir.so umask=077 silent skel=/etc/skel/' $pamcommon
fi

echo "## Personnalisation de l'écran de connexion (lightdm)"
lightdm=/etc/lightdm/lightdm.conf
echo "[SeatDefaults]" > $lightdm
echo "greeter-show-manual-login=true" >> $lightdm
echo "allow-guest=false" >> $lightdm
echo "greeter-show-remote-login=false" >> $lightdm
echo "greeter-hide-users=true" >> $lightdm

echo "## Désactivation du verrouillage écran à la sortie de veille"
#sed -i -e 's/^LOCK_SCREEN/#&/' /etc/default/acpi-support
# On le vire complètement car il pose des problème de pointeur lors de la mise en veille
sudo apt-get remove light-locker

echo "## Modification des répertoires par défaut du home"
sed -i -e 's/^MUSIC/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^DOWNLOAD/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^TEMPLATES/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^PUBLIC/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^DOCUMENTS/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^PICTURES/#&/' /etc/xdg/user-dirs.defaults
sed -i -e 's/^VIDEO/#&/' /etc/xdg/user-dirs.defaults

echo "## Création des liens symboliques"
ln -s /bin/bash /bin/kwartz-sh
ln -s /bin/umount /bin/smbumount

echo "##################################"
echo "#  Bravo ! Intégration terminée  #"
echo "##################################"
echo ""
echo "Noubliez pas de modifier /etc/firefox/syspref.js pour paramétrer globalement Firefox"
