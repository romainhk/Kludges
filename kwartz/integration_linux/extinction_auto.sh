#!/bin/bash
# Script d'extinction automatique des postes sous linux
#
# == Installation sur le client (postes à éteindre) ==
# Sur le poste à éteindre, il fait installer un démon ssh => apt-get install ssh
#
# == Installation sur le serveur (pilote) ==
# Nécessite ldapsearch => apt-get install ldap-utils
# Générer une clé pgp sans mot de passe :
# > ssh-keygen -t rsa
# Puis l'envoyer sur le client :
# > ssh-copy-id -i ~/.ssh/id_rsa.pub USER@IP_ORDI
#
# Personnaliser les premières variables, puis faire un 'crontab -e' et ajouter la ligne :
# 0 19 * * * /usr/local/bin/extinction_auto.sh > /tmp/extinction_auto 2>&1
# pour couper les postes tous les soirs à 19h00
#
# Ne pas oublier de faire un 'chmod 700' sur le script pour plus de sécurité

# Nom et mot de passe d'un compte admin sur l'ordi ciblé
USER=XXX
MDP=XXX
# Liste d'identificateurs des postes à éteindre (nom ou partie de nom)
IDENTIFIANTS=(SALLE208 SALLE209-PROF)
# Adresse ip du serveur kwartz
LDAP=XX.XX.XX.XX
# Le nom du domaine local
DOMAINE=DOMAINE.FR

# == Fin de la personnalisation ==

# Génération du nom de domaine LDAP
OLD_IFS="$IFS"
IFS='.' # permet de spliter la chaine domaine sur chaque .
dom=( $DOMAINE )
for d in ${dom[@]}
do
	DOMAINE_LDAP=$DOMAINE_LDAP,dc=$d
done
DOMAINE_LDAP=${DOMAINE_LDAP#","}
IFS="$OLD_IFS"

# Le nom de l'ordinateur et son groupe sont stockés dans le champ description de la base ldap
# Attention ! Le groupe n'est parfois pas inclus !
for d in `ldapsearch -h $LDAP -b "ou=Computers,$DOMAINE_LDAP" -x description |grep "description:" |cut -d' ' -f3`
do
	for i in "${IDENTIFIANTS[@]}"
	do
		if [[ $d == *$i* ]];then # si le nom de l'ordi contient un identifiant
			ip=`dig $d.$DOMAINE +short`
			if [ -n "$ip" ];then
				echo "Exctinction du poste $ip ($d.$DOMAINE)"
				ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -t -i ~/.ssh/id_rsa $USER@$ip "echo $MDP | sudo -S poweroff && exit"
			else 
				echo "Pas d'IP pour $d.$DOMAINE"
			fi
		fi
	done
done

