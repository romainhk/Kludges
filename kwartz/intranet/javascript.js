var zone_survole='';

function survole(e) {
	if (e != zone_survole) {
		zone_survole = e;
		$(".masque").stop();
		$(".masque").fadeTo(1000, 0.2);
		$("#HG").css('background-image', 'url("tree_printemps.png")');
		$("#HD").css('background-image', 'url("tree_ete.png")');
		$("#BD").css('background-image', 'url("tree_automne.png")');
		$("#BG").css('background-image', 'url("tree_hiver.png")');
  		$("#HG, #HD, #BG, #BD").css('background-position', 'center top');
  		$("#HG, #HD, #BG, #BD").css('background-repeat', 'no-repeat');
		$(e).find('.masque').stop();
		$(e).find('.masque').fadeTo(200, 1);
		$(e).css('background-image', 'none');
	}
}

function toggle_video(t) {
	var id=$(this).attr('id').replace('#', '');
    var src='';
    switch(id) {
        case 'login': src='data/Login.webm'; break;
        case 'logout': src='data/Logout.webm'; break;
        case 'cleusb': src='data/Clef_usb.webm'; break;
    }
    if (src != '') {
        $('#video video source').attr('src', src);
        $('#video video')[0].load();
    }
	zi = $('#video').css('z-index');
	$('#video').css('z-index', -1*zi);
}

$(document).ready(function() {
	$(".masque").fadeTo(0, 0.2);
	$(".video-lnk").on( "click", toggle_video );
    // Pour permettre de cliquer sur la video sans que cela appel toggle_video()
    $('.video-lnk video').click(function(event){
        event.stopPropagation();
    });
});
