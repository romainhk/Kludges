#!/bin/python
# -*- coding: iso8859-15 -*-
import re, sys, codecs, unicodedata, csv
import xml.etree.ElementTree as ET
from collections import OrderedDict
"""
Ce script permet de créer une liste des élèves et des professeur sur kwartz, mais aussi
de déclarer les responsables de groupe pour chaque classe.
Il génère trois fichier qu'il faut importer depuis Kwartz~Control.

== Paramètres d'entrée ==
* nom du groupe kwartz pour les profs
* fichier d'export de SIECLE (ElevesSansAdresses.xml)
* fichier d'export de Kwartz
[* liste des profs par classes (export depuis pronote)]

=== Exportation depuis pronote ===
Depuis un client pronote en mode administrateur :
Fichier > Autres imports/exports > Exporter un fichier texte
* Type de données à exporter -> Services, puis Exporter...

=== Sorties ===
* 3 fichiers textes à importer dans kwartz
* Un quatrième fichier contient les doutes et un résumé des changements de classes

TODO : ajouter aussi en groupe invité
TODO : groupe vide / classe à supprimer
TODO : Pronote: ne trouve pas les profs affectés aux groupes seulement => colonne Groupes mais non standardisé
"""

def xstr(s):
    """ Converti un None en chaine vide """
    if s is None:
        return ''
    return str(s)

def remplacer_accents(ligne):
    """ Supprime les accents du texte source """
    accents = { 'a': ['à', 'À', 'â', 'Â', 'ã'],
                'c': ['ç', 'Ç'],
                'e': ['é', 'É', 'è', 'È', 'ê', 'Ê', 'ë', 'Ë'],
                'i': ['î', 'Î', 'ï', 'Ï'],
                'u': ['ù', 'Ù', 'ü', 'Ü', 'û', 'Û'],
                'o': ['ô', 'Ô', 'ö', 'Ö'] }
    for char, accented_chars in accents.items():
        for accented_char in accented_chars:
            ligne = ligne.replace(accented_char, char)
    return ligne

def creer_login(prenom, nom):
    """ Génère un login valide """
    #if len(nom) + len(prenom) > 25:
    #    prenom = prenom[0]
    login = remplacer_accents('{0}.{1}'.format(prenom, nom)).lower()
    for ch in [' ','-',"'"]: # caractères illégaux
        if ch in login: login=login.replace(ch,"")
    return login

def classe_canon(classe):
    """ Donne le nom cannonique d'une classe (ne doit pas commencer par un chiffre, etc.) """
    if classe is not None :
        if classe[0].isdigit():
            classe = "c"+classe
        return classe.replace("-","").replace(' ', '').lower()
    else:
        return ''

### Lecture des paramètres ###

if len(sys.argv) < 4:
    print("!!! Ce programme nécessite plusieurs paramètres : !!!\n" + \
          "* le nom du groupe kwartz pour les profs\n" + \
          "* le fichier d'export de SIECLE (en xml)\n" + \
          "* le fichier d'export de kwartz\n" + \
          "[* l'export classes-profs issu de pronote]\n" + \
          "\nexemple: python3 crea_kwartz.py GroupeProfs ElevesSansAdresses.xml export_kwartz.txt EXP_PRONOTE.txt")
    sys.exit(1)
groupe_profs = sys.argv[1]
# Fichiers d'entrée
EXPsiecle = codecs.open(sys.argv[2], 'r', encoding='ISO-8859-15')
EXPkwartz = codecs.open(sys.argv[3], 'r', encoding='ISO-8859-15')
# Fichiers de sortie
OUTmodif = codecs.open('Amodifier.txt', 'w', encoding='iso8859-15')
OUTajout = codecs.open('Aajouter.txt', 'w', encoding='iso8859-15')
OUTsuppr = codecs.open('Asupprimer.txt', 'w', encoding='iso8859-15')
OUTmess = codecs.open('Messages.txt', 'w', encoding='iso8859-15')

if not EXPsiecle or not EXPkwartz:
    print("Impossible de lire l'un des fichiers")
    exit(2)
if len(sys.argv) > 4:
    EXPpronote = csv.reader(codecs.open(sys.argv[4], 'r', encoding='utf-16'), delimiter=';')
else:
    EXPpronote = False
### Paramètres globaux ###
#groupe pour les élèves de l'année précédente
groupe_anciens = 'anciens'
#groupes kwartz à ne pas tenir compte
groupes_a_ignorer = [groupe_anciens,'ctx','servinfo','personnel','www', 'secu']
profil_web_prof = 'Professeur'
forcer_maj = False

OUTmess.write("### Lecture des informations ###\n")

def importation_siecle(fichier):
    """ Parse le xml de SIECLE """
    ret = []
    # Parsing
    tree = ET.parse(fichier)
    root = tree.getroot()
    date = root.findtext('.//PARAMETRES/DATE_EXPORT')
    for eleve in root.iter('ELEVE'):
        sortie = eleve.findtext('DATE_SORTIE')
        if sortie and sortie.split('/')[1] == '09':
            # Si la date de sortie est en septembre, il s'agit d'un élève affecté automatiquement au lycée
            # mais qui est parti dans un autre
            # => On laisse tomber
            continue
        eid = eleve.get('ELEVE_ID')
        ine = eleve.findtext('ID_NATIONAL')
        nom = eleve.findtext('NOM_DE_FAMILLE')
        prenom = eleve.findtext('PRENOM')
        naissance = eleve.findtext('DATE_NAISS')
        classe = root.findtext(".//*[@ELEVE_ID='{0}']/STRUCTURE[TYPE_STRUCTURE='D']/CODE_STRUCTURE".format(eid))
        enr = { 'ine': ine, 'nom': nom, 'prenom': prenom, \
                'naissance': naissance, 'classe': classe }
        ret.append(enr)
    return ret

siecle = importation_siecle(EXPsiecle)
EXPsiecle.close()

def importation_kwartz(fichier):
    infos=OrderedDict() # donnée supplémentaire pour les profs
    # format : JOHN;Doe;profs;doe.john;doe.john;;;profs www;profs www;;;;;INE;<LOCAL>;Professeur;
    for ligne in fichier:
        if ligne[0] != '#':
            d = {}
            # Extraction des données vitales
            a = ligne.split(';')
            d['nom'] = a[0].upper()
            #d['nom'] = unicodedata.normalize('NFKD', a[0]).encode('ASCII', 'ignore').decode("utf-8").upper()
            d['prenom'] = a[1].upper()
            d['prenom'] = unicodedata.normalize('NFKD', d['prenom']).encode('ASCII', 'ignore').decode("utf-8").capitalize()
            d['groupe'] = a[2].lower()
            d['login'] = a[3]
            d['droits'] = a[5]
            d['idexterne'] = a[13]
            d['profilweb'] = a[15]
            d['classes'] = a[8].split(' ')
            ine = d['idexterne']
            j = ine
            if ine == '':
                if d['groupe'] not in groupes_a_ignorer and d['groupe'] != groupe_profs:
                    OUTmess.write("Pas d'INE : {nom} {prenom} / {groupe}\n".format(nom=d['nom'], prenom=d['prenom'], groupe=d['groupe']))
                j = d['login']
                d['ine'] = False
            infos[j] = d
    return infos

kwartz = importation_kwartz(EXPkwartz)
EXPkwartz.close()

def importation_pronote(fichier):
    # format : 1S;M. DOE JOHN, Mme TARTELATIE FABIENNE
    rep = {}
    rows = iter(fichier)
    header = rows.__next__()
    # On ne garde que les colonnes CLASSES et PROFS
    mapping = [header.index(x) for x in ['CLASSES','PROFS']]
    REprof = re.compile('M(\.|me)\s+(.+)', re.IGNORECASE|re.UNICODE)
    for ligne in rows:
        classe = ligne[mapping[0]].lower()
        if len(classe) > 0:
            for i in ligne[mapping[1]].split(','): # Plusieurs profs avec la même matière dans la même classe
                n = REprof.search(i)
                if n:
                    nom_complet = n.group(2).strip().replace("'", '').upper()
                    classe = classe_canon(classe)
                    index = nom_complet
                    if index not in rep:
                        rep[index] = [classe]
                    elif classe not in rep[index]:
                        rep[index].append(classe)
    return rep

if EXPpronote:
    pronote = importation_pronote(EXPpronote)

### Traitement ###

# Format d'un enregistrement pour kwartz
form = '{nom};{prenom};{groupe};{login};{login};{password};{droits};;{resp};;;;;{ident};<LOCAL>;{profilweb};\r\n'
# Ajout ou Mise à jour des élèves
OUTmess.write("\n### Ajout et MAJ des élèves ###\n")
for e in siecle:
    if e['ine'] is None :
        OUTmess.write('Élève sans INE ! {0} {1}\n'.format(e['nom'], e['prenom']))
    else:
        ine = e['ine']
        nom = remplacer_accents(e['nom'])
        prenom = remplacer_accents(e['prenom'])
        classe = classe_canon(e['classe'])
        if ine in kwartz:
            # Élèves déjà présent -> maj
            kel = kwartz[e['ine']]
            if classe == '':
                #classe = classe_canon(kel['groupe'])
                continue
            if forcer_maj or classe != kel['groupe'] or kel['idexterne'] == '':
                # ... si la classe a changé ou s'il n'y a pas d'identifiant externe
                OUTmodif.write(form.format(nom=nom, prenom=prenom, login=kel['login'],
                    groupe=classe, password='', droits=kel['droits'], resp='',
                    ident=ine, profilweb=kel['profilweb']))
            # Traité donc on le retire de kwartz
            kwartz.pop(ine, None)
        else:
            # Nouvel élèves -> ajout
            login = creer_login(prenom, nom)
            n = e['naissance']
            password = '{0}{1}{2}'.format(n[:2] , n[3:5] , n[-2:]) # date de naissance JJMMAA
            if classe == '':
                OUTmess.write('{0} - {1} : nouvel élève sans classe !\n'.format(ine, login))
                continue
            #
            OUTajout.write(form.format(nom=nom, prenom=prenom, groupe=classe,
                login=login, password=password, droits='', resp='',
                ident=e['ine'], profilweb=''))

# Suppression des élèves de l'année précédente et MAJ des comptes profs
OUTmess.write("\n### Suppression des anciens comptes et résumé des changements d'affectation prof ###\n")
for ine,f in sorted(kwartz.items()):
    if f['groupe'] not in groupes_a_ignorer:
        if EXPpronote and f['groupe'] == groupe_profs: # C'est un prof !
            index = '{0} {1}'.format(f['nom'], f['prenom']).upper()
            # On regarde s'il est toujours en service
            if index in pronote:
                intersection = list(set(pronote[index]) ^ set(f['classes']))
                # S'il y a quelquechose dans intersection => de nouvelles classe à affecter
                if forcer_maj or len(intersection) > 0 or f['idexterne'] == '':
                    classes = ' '.join(pronote[index])
                    OUTmodif.write(form.format(nom=f['nom'], prenom=f['prenom'], login=f['login'],
                        groupe=groupe_profs, password='', droits=f['droits'], resp=classes,
                        ident=index, profilweb=f['profilweb']))
                    # Résumé des changements d'affectation
                    ajout = ' +'.join(set(pronote[index])-set(f['classes']))
                    retrait = ' -'.join(set(f['classes'])-set(pronote[index]))
                    OUTmess.write("{prof}: +{ajout} // -{retrait}\n".format(prof=index, ajout=ajout, retrait=retrait))
                pronote.pop(index, None)
        else:
            # On désactive le compte
            # Sauf si c'est un ufa, et sauf si c'est un profs alors qu'on a pas d'affectation pronote
            if f['groupe'][:3] != 'ufa' and (EXPpronote and f['groupe'] != groupe_profs):
                OUTsuppr.write(form.format(nom=f['nom'], prenom=f['prenom'], login=f['login'],
                    groupe=groupe_anciens, password='', droits=f['droits'], resp='',
                    ident=ine, profilweb=f['profilweb']))

# Ajout des nouveaux profs
if EXPpronote:
    OUTmess.write("\n### Ajout des nouveaux profs ###\n")
    for i,p in pronote.items():
        a = i.split(' ')
        classes = ' '.join(p)
        nom = a[0]
        if len(a) > 1: prenom = a[1]
        else: prenom = ''
        login = creer_login(prenom, nom)
        if len(a) > 2: # Nom complexe !
            OUTmess.write("\nNom compose! Veuillez controler la ligne d'importation suivante et l'ajouter dans Amodifier.txt si besoin :\n".format(a))
            OUTmess.write(form.format(nom=nom, prenom=prenom, groupe=groupe_profs,
                login=login, password=nom, droits='', resp=classes,
                ident=i, profilweb=profil_web_prof))
            OUTmess.write('\n')
            continue
        password = nom.replace("-","").replace(' ', '')
        OUTajout.write(form.format(nom=nom, prenom=prenom, groupe=groupe_profs,
            login=login, password=password, droits='', resp=classes,
            ident=i, profilweb=profil_web_prof))
        # Résumé des nouvelles d'affectations
        ajout = ' +'.join(p)
        OUTmess.write("{prof}: +{ajout}\n".format(prof=i, ajout=ajout))
else:
    OUTmess.write("Pas de fichier pronote fournit : Pas de création/affectation de comptes prof\n")

OUTmess.write("\n## Attention !! ##\nLe mot de passe par défaut des nouveaux comptes profs est leur nom de famille en majuscule. Ne pas oublier de cocher \"Forcer la modification du mot de passe\" lors de l\'importation.")
OUTajout.close()
OUTsuppr.close()
OUTmodif.close()
OUTmess.close()
