#!/usr/bin/env python
# -*- coding: utf-8  -*-

"""
Access data from mongodb through a timed cache.
"""

from __future__ import unicode_literals

from pymongo import MongoClient
import random
import string
from time import time

DEFAULT_TIMEOUT = 60


class EtherealData(object):
    """
    Representation of a data in mongodb, as seen recently.
    """

    _data = {}
    _last_update = 0

    def __init__(self, collection, filter_={}, timeout=DEFAULT_TIMEOUT):
        """
        :param Collection collection: a mongo collection object
        :param dict filter_: subset of the collection to access to
        :param int timeout: time before invalidating data
        """
        self.collection = collection
        self.filter_ = filter_
        self.timeout = timeout
        self._update_cache()

    def get(self, value):
        """
        Update cache if needed and retreive a value.

        :param value: th value to insert
        :rtype: builtin type|None
        """
        if int(time()) - self._last_update > self.timeout:
            self._update_cache()

        return self._data.get(value, None)

    def set(self, key, value):
        """
        Insert in db the key/value pair.

        :param key: the key
        :param value: the value
        """
        self._data[key] = value
        self.collection.update(self.filter_, {'$set': {key: value}}, upsert=True)

    #def set_all(self, dict_):
    #    """
    #    Insert in db a all dict.
    #    :param dict:
    #    """
    #    self._update_cache()
    #    self._data[key] = value
    #    self.collection.update(self.filter_, self._data, upsert=True)

    def _update_cache(self):
        """
        Update local cache from db.
        """
        #print('>> Updating cache')
        values = self.collection.find_one(self.filter_)
        if values is None:
            values = {}
        self._data = values
        self._last_update = int(time())

if __name__ == '__main__':
    mongo = MongoClient()
    collection = mongo.test.mycollection
    ed = EtherealData(collection=collection)

    print('Truc: {}'.format(ed.get('truc')))
    data = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(7))
    ed.set('truc', data)
    print('Truc: {}'.format(ed.get('truc')))
