CREATE TABLE `Rapports` (
	`Timestamp`	TEXT NOT NULL,
	`Semaine`	INTEGER DEFAULT 0,
	`Station`	TEXT NOT NULL,
	`User`	TEXT DEFAULT '',
	`Problème`	TEXT DEFAULT '',
	`Commentaire`	TEXT DEFAULT '',
	`Delta`	INTEGER DEFAULT 0, -- en secondes, temps mis pour répondre
	`Statut` INTEGER DEFAULT 0, -- 0=non traité, en cours/en attente=1, impossible=2, terminé=3
	PRIMARY KEY(Timestamp)
);
CREATE TABLE `Connexions` (
	`Timestamp`	TEXT NOT NULL,
	`Salle`	TEXT NOT NULL,
	`Prof`	TEXT DEFAULT '',
	`Logout`	INTEGER DEFAULT 0,
	PRIMARY KEY(Timestamp)
);
