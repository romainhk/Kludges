#!/bin/python
# -*- coding: utf-8 -*-
#
# TODO :
# * accès limité ? par un champ "secret" ?
import os, sys
import threading
import time, datetime
import logging
import signal
import cgi, json
import http.server
import sqlite3
import argparse
import collections # OrderedDict
from urllib.parse import urlparse, parse_qs
import socket

# ***** Paramètres *****
parser = argparse.ArgumentParser(prog='denominateur')
parser.add_argument('--port', type=int, default=8000, help='Port découte')
parser.add_argument('address', nargs='?', default='', help='Plage d\'addresse écoutée')
args = parser.parse_args(sys.argv[1:])

# ***** La base de données *****
nom_base = 'rapports.sqlite'
try:
    conn = sqlite3.connect(nom_base, check_same_thread=False)
except:
    logging.error("Impossible de se connecter à la base de données ({0})".format(nom_base))
    sys.exit(3)
conn.row_factory = sqlite3.Row
curs = conn.cursor()

# ***** Le serveur *****

class Denominateur(http.server.HTTPServer):
    def __init__(self, address, handler):
        super().__init__(address, handler)
        self.fichiers_proteges = [ nom_base, "structure.sql" ]
        self.header = ['Timestamp', 'Station', 'User', 'Problème', 'Commentaire', 'Delta', 'Statut']
        self.derniere_liste = {}

    def terminaison(self, signal):
        logging.info('Extinction du serveur (sig {0})'.format(signal))
        conn.close()
        assassin = threading.Thread(target=self.shutdown)
        assassin.daemon = True
        assassin.start()

def term(signal, frame):
    """ Lance l'extinction """
    serveur.terminaison(signal)

# ***** Le handler HTTP *****

class ServerHandler(http.server.SimpleHTTPRequestHandler):

    def liste_semaines(self):
        # Liste les semaines connues
        data = collections.OrderedDict()
        req = 'SELECT DISTINCT Semaine, Timestamp FROM Rapports GROUP BY Semaine'
        for row in curs.execute(req).fetchall():
            d = dict(zip(row.keys(), row))
            semaine = d['Semaine']
            if semaine is not None:
                date = datetime.datetime.strptime(d['Timestamp'], '%Y-%m-%d %H:%M:%S.%f')
                lundi = date - datetime.timedelta(days = date.weekday()) # la date du lundi
                dimanche = date + datetime.timedelta(days = (6-date.weekday()))
                data[semaine] = '# {0} : {1} / {2}'.format(semaine,
                        lundi.strftime('%d-%m'),
                        dimanche.strftime('%d-%m'))
        return data

    def generer_liste_pb(self, semaine):
        # Génère la liste des problèmes (page principale)
        #La semaine 0 veut dire toutes les semaines
        data = collections.OrderedDict()
        where = ''
        if semaine > 0 and semaine < 54:
            where = 'WHERE Semaine={0}'.format(semaine)
        req = 'SELECT rowid,* FROM Rapports {0} ORDER BY Timestamp'.format(where)
        for row in curs.execute(req).fetchall():
            d = dict(zip(row.keys(), row))
            if d['Problème'] is None: d['Problème'] = ''
            if d['Commentaire'] is None: d['Commentaire'] = ''
            data[d['rowid']] = d
        self.server.derniere_liste = data
        r = ''
        for index,d in data.items():
            s = ''
            class_css = 'statut-{0}'.format(d['Statut'])
            for h in self.server.header:
                val = d[h]
                if h == 'Statut': # Traduction du statut
                    if   val == 0: val = ''
                    elif val == 1: val = 'En cours'
                    elif val == 2: val = 'Impossible'
                    elif val == 3: val = 'Terminé'
                elif h == 'Delta' and val is None:
                    class_css = 'non-repondu'
                    val = 99999
                s = s + '<td>{0}</td>'.format(val)
            r = r + '<tr id="{0}" class="{1}">{2}</tr>\n'.format(index, class_css, s)
        return r

    def generer_stats(self, page):
        # Génère les pages de statistiques
        data = collections.OrderedDict()
        champs_a_afficher = ['nb_rapports','temps_rep_moy','nouveau','non_repondu','en_cours','impossible','termine']
        if page == "general":
            req = """SELECT rowid,count(*) AS nb_rapports,
            sum(delta)/sum(CASE WHEN "Statut"=0 AND "Delta" IS NULL THEN 0 ELSE 1 END) AS temps_rep_moy,
            sum(CASE WHEN "Statut"=0 AND "Delta" IS NOT NULL THEN 1 ELSE 0 END) AS nouveau,
            sum(CASE WHEN "Statut"=0 AND "Delta" IS NULL THEN 1 ELSE 0 END) AS non_repondu,
            sum(CASE WHEN "Statut"=1 THEN 1 ELSE 0 END) AS en_cours,
            sum(CASE WHEN "Statut"=2 THEN 1 ELSE 0 END) AS impossible, 
            sum(CASE WHEN "Statut"=3 THEN 1 ELSE 0 END) AS termine
            FROM Rapports ORDER BY nb_rapports DESC
            """
        elif page == "poste" or page == "utilisateur":
            if page == "poste":
                champ = "station"
            else:
                champ = "user"
            champs_a_afficher = [champ] + champs_a_afficher
            req = """select rowid,{0} AS {0},count(*) AS nb_rapports,
            IFNULL(sum(delta)/sum(CASE WHEN "Statut"=0 AND "Delta" IS NULL THEN 0 ELSE 1 END),99999) AS temps_rep_moy,
            sum(CASE WHEN "Statut"=0 AND "Delta" IS NOT NULL THEN 1 ELSE 0 END) AS nouveau,
            sum(CASE WHEN "Statut"=0 AND "Delta" IS NULL THEN 1 ELSE 0 END) AS non_repondu,
            sum(CASE WHEN "Statut"=1 THEN 1 ELSE 0 END) AS en_cours,
            sum(CASE WHEN "Statut"=2 THEN 1 ELSE 0 END) AS impossible, 
            sum(CASE WHEN "Statut"=3 THEN 1 ELSE 0 END) AS termine
            FROM Rapports GROUP BY {0} ORDER BY nb_rapports DESC""".format(champ)
        else: return ""
        for row in curs.execute(req).fetchall():
            d = dict(zip(row.keys(), row))
            data[d['rowid']] = d
        r = ''
        for index,d in data.items():
            s = ''
            for h in champs_a_afficher:
                val = d[h]
                try:
                    i = ['nouveau','en_cours','impossible','termine'].index(h)
                    classe = ' class="statut-{0}"'.format(i)
                except:
                    classe = ''
                s = s + '<td{1}>{0}</td>'.format(val, classe)
            r = r + '<tr id="{0}">{1}</tr>\n'.format(index, s)
        return r

    def log_eleve(self):
        # Dit si un eleve est dans une salle surveillée
        ip = self.client_address[0]
        fqdn = socket.gethostbyaddr(ip)[0]
        station = fqdn.split('.')[0]
        salle = fqdn.replace(station, '').replace('jeanmoulin.fr', '').replace('.', '')

        req = 'SELECT * FROM Connexions WHERE Salle=? AND Logout<>? ORDER BY Timestamp DESC LIMIT 0,20'
        donnees = ( salle, -1 )
        a = curs.execute(req, donnees).fetchone()
        if a is not None:
            d = dict(zip(a.keys(), a))
            print(d)
            now = datetime.datetime.now()
            login = datetime.datetime.strptime(d['Timestamp'], '%Y-%m-%d %H:%M:%S.%f')
            if now - login < datetime.timedelta(seconds=7200):
                return 'OUI'
        return 'NON'

    def log_prof(self, user, logout=0):
        # Enregistre la connexion/deconnexion d'un prof
        timestamp = datetime.datetime.now()
        ip = self.client_address[0]
        fqdn = socket.gethostbyaddr(ip)[0]
        station = fqdn.split('.')[0]
        salle = fqdn.replace(station, '').replace('jeanmoulin.fr', '').replace('.', '')

        req = 'INSERT or REPLACE INTO Connexions ' \
            + '(Timestamp, Salle, Prof, Logout) ' \
            + 'VALUES (?, ?, ?, ?)'
        donnees = ( str(timestamp), salle, user, logout )
        try:
            curs.execute(req, donnees)
        except sqlite3.Error as e:
            logging.error("Insertion d'une connexion : {0}\n{1}".format(e.args[0], req))
            logging.error(donnees)
            return False
        conn.commit()

    def do_GET(self):
        #logging.warning(self.headers)
        params = urlparse(self.path)
        query = parse_qs(params.query)
        if   params.path == '/liste-pb':
            semaine = int(query.get('semaine', ['0']).pop())
            self.repondre({'html':self.generer_liste_pb(semaine)})
        elif params.path == '/liste-semaines':
            #self.repondre({'html':self.liste_semaines()})
            self.repondre(self.liste_semaines())
        elif params.path == '/stats':
            page = query.get('page', ['']).pop()
            self.repondre({'html':self.generer_stats(page)})
        elif params.path == '/maj-statut':
            num = query.get('num', ['']).pop()
            val = int(query.get('val', ['99']).pop())
            if val == 4: # SUPPR
                rep = self.suppr_enregistrement(int(num))
            else:
                rep = self.maj_statut(num, val)
            self.repondre('OUI')
        elif params.path == '/eleve':
            ## TEST ##
            # Un élève a t-il le droit de se connecter dans la salle ?
            #rep = self.log_eleve()
            rep = "OUI"
            self.repondre(rep)
        elif params.path == '/logoff-prof':
            ## TEST ##
            # Le prof s'est déconnecté
            user = query.get('user', ['']).pop()
            self.log_prof(user, 1)
            self.repondre('OUI')
        elif params.path[1:] not in self.server.fichiers_proteges:
            http.server.SimpleHTTPRequestHandler.do_GET(self)
        else:
            logging.error("Acces interdit à la ressource {0}".format(params.path))

    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })
        rep = 'NON'
        if self.path == '/info':
            data = {}
            data['timestamp'] = form.getvalue('timestamp')
            data['station'] = form.getvalue('station').lower()
            data['user'] = form.getvalue('user')
            data['problème'] = form.getvalue('problème')
            data['commentaire'] = form.getvalue('commentaire')
            data['delta'] = form.getvalue('delta')
            data['statut'] = 0
            if data['problème'] is None and data['delta'] is not None:
                # Pas de problème ! -> Clôture
                data['statut'] = 3
            if data['problème'] == '...' and data['commentaire'] is None:
                # Validation d'un problème sans précisions -> Clôture
                data['statut'] = 3
            self.maj_rapport(data)
            self.log_prof(data['user'], 0)
            rep = 'OUI'
        self.repondre(rep)

    def repondre(self, reponse):
        """
        Envoie une réponse http [sic]
        :param reponse: la réponse
        :type reponse: objet, généralement str ou dictionnaire
        """
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(reponse), 'UTF-8'))
        self.wfile.flush()

    def maj_rapport(self, data):
        """ Insère un rapport complet dans la base
        """
        logging.warning(data)
        semaine = datetime.datetime.strptime(data['timestamp'], '%Y-%m-%d %H:%M:%S.%f').isocalendar()[1]
        req = 'INSERT OR REPLACE INTO Rapports ' \
            + '(Timestamp, Semaine, Station, User, Problème, Commentaire, Delta, Statut) ' \
            + 'VALUES (?, ?, ?, ?, ?, ?, ?, ?)'
        donnees = ( data['timestamp'],      semaine,            data['station'],
                    data['user'],           data['problème'],   data['commentaire'],
                    data['delta'],          data['statut'] )
        try:
            curs.execute(req, donnees)
        except sqlite3.Error as e:
            logging.error("Insertion d'un rapport : {0}\n{1}".format(e.args[0], req))
            logging.error(donnees)
            return False
        conn.commit()

    def suppr_enregistrement(self, num):
        """ Supprime un enregistrement de la base
        """
        logging.warning("## Suppression de l'enregistremment {0} ##".format(num))
        if num in self.server.derniere_liste: # Copie texte de l'enregistrement supprimé
            logging.warning(self.server.derniere_liste[num])

        req = 'DELETE FROM Rapports WHERE rowid=?'
        donnees = ( num, )
        try:
            curs.execute(req, donnees)
        except sqlite3.Error as e:
            logging.error("Suppression d'un rapport : {0}\n{1}".format(e.args[0], req))
            logging.error(donnees)
            return False
        conn.commit()

    def maj_statut(self, num, val):
        """ Met à jour le statut d'un rapport
        """
        req = 'UPDATE Rapports SET Statut=? WHERE rowid=?'
        donnees = ( val , num )
        try:
            curs.execute(req, donnees)
        except sqlite3.Error as e:
            logging.error("MAJ d'un statut : {0}\n{1}".format(e.args[0], req))
            logging.error(donnees)
            return 'NON'
        conn.commit()
        return 'OUI'

# ***** Init *****

if __name__ == "__main__":
    # Logging
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    # Logging fichier
    file_handler = logging.FileHandler('denominateur.log', 'a')
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s;%(levelname)s;%(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    # Logging console
    steam_handler = logging.StreamHandler()
    steam_handler.setLevel(logging.DEBUG)
    logger.addHandler(steam_handler)
    # Fichier de config
    serveur = Denominateur( (args.address, args.port) , ServerHandler)
    thread = threading.Thread(target = serveur.serve_forever)
    thread.deamon = True
    logging.info('Démarrage du serveur (PID {1}) sur le port {0}'.format(args.port, os.getpid()))
    time.sleep(0.2)
    thread.start()
    # Interception des signaux d'extinction (2 et 15)
    signal.signal(signal.SIGINT, term)
    signal.signal(signal.SIGTERM, term)

