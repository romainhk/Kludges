// Variables globales
var adresse_serveur = '172.16.1.113';
var port_serveur = '8000';

function liste_problemes() {
    /* Affiche la liste des problèmes */
    $("#pb").show();
    $("#stats").hide();
    var semaine = $("#semaine").val();
    $.get( "/liste-pb?semaine="+semaine, function( data ) {
        $('#liste-pb > tbody').html( data['html'] );
        $('#liste-pb').stupidtable();
        //toggle_clos();
        $("#liste-pb td:nth-child(7)").click(cell_to_select);
    });
}

function stats_onglet(page) {
    /* Gère les sous-onglets de la partie stats */
    $("#stats-general").hide();
    $("#stats-utilisateur").hide();
    $("#stats-poste").hide();
    $.get( "/stats?page="+page, function( data ) {
        ou = '';
        if (page == "general") {
            ou = "stats-general";
        } else if (page == "poste") {
            ou = "stats-poste";
        } else if (page == "utilisateur") {
            ou = "stats-utilisateur";
        } else { return false; }
        $("#"+ou).show();
        $('#'+ou+' > tbody').html( data['html'] );
        $('#'+ou).stupidtable();
    });
}

function stats() {
    /* Affiche la page de stats */
    $("#pb").hide();
    $("#stats").show();
}

function toggle_clos() {
    /* Affiche/Masque les éléments clos de la liste des problèmes */
    $(".statut-2").toggle();
    $(".statut-3").toggle();
    $(".non-repondu").toggle();
}

function cell_to_select(e) {
    /* Ajoute un select au tableau pour pouvoir mettre à jour les données */
    cell = $(e.target);
    s = cell.find('select');
    // S'il n'y a pas encore de select et si le click vient d'une cellule
    if (cell.prop("tagName") == "TD" && s.length == 0) {
        valeurs = ['Nouveau', 'En cours', 'Impossible', 'Terminé', 'SUPPR'];
        selected = cell.html();
        cell.html('');
        var sel = $('<select>').appendTo(cell);
        $.each(valeurs, function(i, j) {
            pardefaut = "";
            if (j == selected) { pardefaut = ' selected="selected"' ; }
            sel.append('<option value="'+i+'"'+pardefaut+'>'+j+'</option>');
        });
        sel.change( function(){
            // Au changement de valeur, on l'enregistre dans la base
            val = $(this).val();
            txt = $('option:selected', this).text(); // option sélectionnée fils de l'élément this
            num = cell.closest('tr').attr('id');
            index = parseInt(val);
            $.get( 'http://'+adresse_serveur+':'+port_serveur+'/maj-statut?num='+num+'&val='+index, function( data ) {
                c = cell.parents('tr');
                c.removeClass();
                if (data == 'OUI' && index >= 0 && index <= 3) { c.addClass("statut-"+index); }
                else if (data == 'NON') { c.addClass("maj-non"); }
                cell.parents("td").html(txt); // Et on retire le select
            });
        });
    }
}

$(document).ready(function() {
    $("#pb").hide();
    $("#stats").hide();
    $("#stats-utilisateur").hide();
    $("#stats-poste").hide();
    // La liste des semaines
    $.get( "/liste-semaines", function( data ) {
        for (var i = 1; i <= 53 ; i++) {
            j = (33 + i)%53; // on affiche les semaines à partir d'aout
            if (j in data) {
                select = '<option value="'+j+'">'+data[j]+'</options>';
                $("#semaine").append(select);
            }
        }
        $("#semaine option:last-child").prop('selected', true);
        liste_problemes();
    });
});
