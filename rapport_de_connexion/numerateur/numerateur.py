﻿#/bin/python
# -*- coding: utf-8 -*-
#
# numerateur génère un rapport qu'il envoie au dénominateur
#
# Note pour WinPython :
# Pour pouvoir tester le programme, il faut lancer "WinPython CommandPrompt",
# puis faire un "set PYTHONPATH=C:\WinPython-32bit-3.4.2.2\python-3.4.2"
#TODO: retirer le bouton valider ?

import os, sys, tkinter
import getpass, platform, socket, requests, json
import datetime
import argparse

# Création de la fenêtre principale
top = tkinter.Tk()
top.grid()
# ========== Interception de l'extinction ==========
def term():
    """ Terminaison non voulue => logout """
    payload['commentaire'] = 'LOGOUT'
    timestamp2 = datetime.datetime.now()
    payload['delta'] = int( (timestamp2 - timestamp).total_seconds() )
    envoyer_post(payload)

    os.system("shutdown -l")
    #os.system("logout")
    top.destroy()
top.protocol("WM_DELETE_WINDOW", term)

# =================== Paramètres ===================
parser = argparse.ArgumentParser(prog='numerateur')
parser.add_argument('--port', type=int, default=8000, help='Port découte sur le dénominateur')
parser.add_argument('address', default='', help='Addresse du denominateur')
args = parser.parse_args(sys.argv[1:])

if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(__file__)

# ==================== Données =====================

PROBLEMES = [
    "...",
    "Connexion impossible",
    "Problème de projection",
    "Poste(s) non fonctionnel(s)",
    "Manque souris/clavier",
    "Manque de l'encre",
    "Imprimante non installée",
    "Problèmes d'impression",
    "Un logiciel ne fonctionne pas",
    "Autre"
]

# Adresse de contact du serveur (dénominateur)
denominateur = 'http://{0}:{1}/info'.format(args.address, args.port)
user = getpass.getuser()
#station = os.environ['COMPUTERNAME']
#station = platform.node()
station = socket.gethostname()
timestamp = datetime.datetime.now()

payload = {
    'user' : user,
    'station' : station,
    'timestamp': timestamp
}

# Pour ignorer les paramétrages de proxy de windows (vu qu'on reste en interne)
proxyDict = { 
    "http"  : '', 
    "https" : '', 
    "ftp"   : ''
}

# =================== Traitement ==================

def envoyer_post(payload):
    """ Envoie une requete post au serveur """
    try:
        r1 = requests.post(denominateur, data=payload, proxies=proxyDict)
    except requests.exceptions.ConnectionError as e:
        print("Impossible de connecter au serveur {0}".format(denominateur))
        print(payload)
        sys.exit(1)
    reponse = r1.text
    status = r1.status_code
    #print("{0} - {1}".format(status, reponse))

def envoi_final():
    """ Seconde requête, plus complète """
    payload['problème'] = valProb.get()
    payload['commentaire'] = valComm.get()

    timestamp2 = datetime.datetime.now()
    payload['delta'] = int( (timestamp2 - timestamp).total_seconds() )
    
    envoyer_post(payload)
    sys.exit(0)

envoyer_post(payload)

# ============== Interface graphique ==============

def statut_pb():
    """ Boutton Problèmes """
    # Détail du problème
    labProb = tkinter.StringVar()
    labProb.set("Nature du problème :")
    label = tkinter.Label(top,textvariable=labProb, anchor="w", bg=bg)
    label.grid(column=1,row=2,columnspan=2,sticky='EW')
    valProb.set(PROBLEMES[0])
    saisie1 = tkinter.OptionMenu(top, valProb, *PROBLEMES)
    saisie1.config(bg=bg)
    saisie1["menu"].config(bg=bg)
    saisie1.grid(column=1,row=3,columnspan=2,padx=16,pady=4,sticky='EW')
    # Commentaire
    labComm = tkinter.StringVar()
    labComm.set("Commentaire éventuel :")
    label = tkinter.Label(top,textvariable=labComm, anchor="w", bg=bg)
    label.grid(column=1,row=4,sticky='EW')
    saisie2 = tkinter.Entry(top, textvariable=valComm, bg=bg)
    saisie2.grid(column=1,row=5,columnspan=2,padx=16,pady=4,sticky='EW')
    # Bouton de Validation
    button = tkinter.Button(top, bg=bg, borderwidth=3, text=u"Valider", command=envoi_final)
    button.grid(column=2,row=6,columnspan=4)

bg = 'bisque'
top['bg']=bg # couleur de fond
# Logo
logo = tkinter.PhotoImage(file=os.path.join(application_path,"logo.png"))
w = tkinter.Label(top, image=logo, bg=bg)
w.grid(row=0, column=0, rowspan=5, sticky='EW', padx=7, pady=7)

labEntete = tkinter.StringVar()
labEntete.set("Votre salle informatique est-elle fonctionnelle ?")
label = tkinter.Label(top,textvariable=labEntete, anchor="w", bg=bg)
label.grid(column=1,row=0,columnspan=2,sticky='EW')
# Statut
img_ok = tkinter.PhotoImage(file=os.path.join(application_path,"ok.png"))
w = tkinter.Button(top, bg=bg, image=img_ok, width=54, command=envoi_final)
w.grid(row=1, column=1, sticky='EW', padx=3, pady=3)
img_pb = tkinter.PhotoImage(file=os.path.join(application_path,"pb.png"))
w = tkinter.Button(top, bg=bg, image=img_pb, width=54, command=statut_pb)
w.grid(row=1, column=2, sticky='EW', padx=3, pady=3)
#
valProb = tkinter.StringVar()
valComm = tkinter.StringVar()

# Paramètres généraux
top.grid_columnconfigure(0,weight=1)
top.resizable(False,False) # Pas de redimentionnement horizontal ou vertical possible
top.update()
#top.geometry(top.geometry()) # Taille de fenêtre automatique et fixe
top.title('Déclaration de problèmes') # Titre de la fenêtre
top.mainloop()
