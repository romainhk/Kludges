#!/bin/python3
# -*- coding: utf-8 -*-
# Lancer la compilation avec la commande 'python setup.py py2exe'
from distutils.core import setup
import py2exe

includefiles = [('', ['logo.png', 'ok.png', 'pb.png',
        'C:\\Windows\\System32\\msvcp100.dll',
        'C:\\Windows\\System32\\msvcr100.dll'])]
# On bundle aussi les dll VC++ redist 2010 au cas où ils ne seraient pas présent sur l'ordi

setup(
    windows=[{
		'script': 'numerateur.py',
        'icon_resources': [(1, "icon.ico")]
	}],
    options = {'py2exe': {
		'bundle_files': 2,
		'compressed': True,
		'dist_dir':'numerateur'
	}},
    data_files = includefiles,
    zipfile = None
)
