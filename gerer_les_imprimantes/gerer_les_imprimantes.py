#!c:/Python27/python.exe -u
# -*- coding: utf8 -*-
import win32print, subprocess, socket, re, os, sys, ConfigParser, logging, string, argparse
"""
				****  Gerer les imprimantes  ****
	Ajoute & Supprime les imprimantes nécessaires pour un ordinateur
		
	Paramètre :
	* dossier réseau où se situe l'exe, les cfg et les vbs. Ex: \\kwartz-server\netlogon\wpkg\software\imprimantes

	Paramètres optionnelles :
	* defaults : Faut uniquement l'affécation par défaut des imprimantes.
		Une imprimante est mise sur un ordinateur x lorsqu'il est noté un * devant le nom de ce poste dans le champ affectation de config.cfg
	* purge : Supprime toutes les imprimantes (sauf ceux de ignore_ptr) avant d'en installer de nouvelles
	* est64bits : Permet de désigner un ordinateur 64 bits (typiquement un windows 7)
		Dans ce cas, les champs inf et driver de modeles.cfg sont ignorés au profit de inf64 et driver64, pour pouvoir donner un autre driver
		Si inf64 vaut *, le driver est considéré déjà présent sur le poste
	
	Problèmes connus:
	En mode IP, l'imprimante doit etre démarrée avant le lancement du script! (sinon, la résolution pour trouver l'ip lors de la première installation n'aboutie pas) -> passer par dns ?
"""
def nslook(ip):
	""" Retourne le nom et le groupe associé à une IP
		Spécifique à DOS (pas bien !)
	"""
	fqdn = ['', '']
	nslookup = str(subprocess.check_output('nslookup {ip}'.format(ip=ip)))
	REhost = re.compile('Nom\s+:\s+(.+)Address', re.LOCALE|re.DOTALL)
	n = REhost.search(nslookup)
	if n:
		fqdn = n.group(1).replace(r'\r\n', '').split('.')
	else:
		logging.error("Impossible de trouver le groupe pour {hote}".format(hote=ip))
	return (fqdn[0], fqdn[1]) # hote, groupe

"""
import dns.resolver
def requete_dns(nom):
	# Résolution DNS : fqdn => IP
	answers_IPv4 = dns.resolver.query(nom, 'A')
	return answers_IPv4.address.pop()
"""
	
def ajouter_ptr(nom):
	""" Ajoute une imprimante, par son IP ou par son UNC (partage SMB)
	"""
	ptr = imprimantes[nom]
	if ptr['modele'] in modeles:
		mod = modeles[ptr['modele']]
	else:
		logging.error("Le modèle '{mdl}' n'existe pas.".format(mdl=ptr['modele']))
		return False
	if est64bits:
		if 'inf64' in mod and 'driver64' in mod:
			inf = mod['inf64']
			drv = mod['driver64']
		else:
			logging.error("Le driver 64bits de '{mdl}' n'a pas été correctement définit.".format(mdl=mod))
			return False
	else:
		inf = mod['inf']
		drv = mod['driver']

	if ptr.has_key('unc'):
		logging.info("Ajout de '{nom}' par UNC".format(nom=nom))
		#win32print.AddPrinter(Name, Level , pPrinter )
		#inst = 'rundll32 printui.dll,PrintUIEntry /in /q /n "{unc}"'.format(unc=ptr['unc'])
		inst = 'rundll32 printui.dll,PrintUIEntry /if /u /b "{nom}" /f "{inf}" /r "{unc}" /m "{drv}"'.format(
			nom=nom, inf=chemin_inf+inf, unc=ptr['unc'], drv=drv)
	else:
		logging.info("Ajout de '{nom}' par IP".format(nom=nom))
		if est64bits: nom2 = nom.split('.')[0]
		else: nom2 = nom
		# Ajout du port IP
		try:
			ip = socket.gethostbyname(nom2)
		except:
			logging.error("Impossible de retrouver l'IP de {nom}".format(nom=nom2))
			return False
		inst = 'cscript {path}prnport.vbs -a -r IP_{ip} -h {ip} -o raw -n 9100'.format(ip=ip, path=scriptpath)
		logging.debug("Installation IP : {chaine}".format(chaine=inst))
		try:
			subprocess.check_call(inst)
		except:
			logging.error("Impossible de créer le port IP : {inst}".format(inst=inst))
			return False

		if inf != "*":
			# Installation du driver .inf
			# Si inf vaut *, aller il s'agit d'un driver déjà présent sur l'ordi -> pas d'installation
			chemin = os.path.dirname(chemin_inf+inf)
			inst = 'cscript {path}prndrvr.vbs -a -i "{inf}" -h "{chemin}" -m "{drv}"'.format(path=scriptpath, inf=chemin_inf+inf, chemin=chemin, drv=drv)
			logging.debug("Installation DRV : {chaine}".format(chaine=inst))
			try:
				subprocess.check_call(inst)
			except:
				logging.error("Impossible d'installer le driver : {inst}".format(inst=inst))
				return False

		inst = 'cscript {path}prnmngr.vbs -a -p "{nom}" -r "IP_{ip}" -m "{drv}"'.format(path=scriptpath, nom=nom, ip=ip, drv=drv)

	logging.debug("Installation PTR : {chaine}".format(chaine=inst))
	try:
		subprocess.check_call(inst)
	except subprocess.CalledProcessError:
		logging.critical("Erreur "+returncode+" lors de l'installation de l'imprimante : "+cmd+"\r\n"+output)
	logging.debug("--- {nom} installé ---".format(nom=nom))

def supprimer_ptr(nom):
	""" Supprime une imprimante
	"""
	if nom not in ignorer_ptr:
		logging.info("Suppression de '{nom}'".format(nom=nom))
		try:
			subprocess.check_call('cscript {path}prnmngr.vbs -d -p "{nom}"'.format(nom=nom, path=scriptpath))
		except:
			logging.error("Impossible de supprimer {nom}".format(nom=nom))
			
def default_ptr(nom):
	""" Définit l'imprimante par défaut
	"""
	logging.info("{nom} sera l'imprimante par défaut".format(nom=nom))
	#win32print.SetDefaultPrinter(nom)
	try:
		subprocess.check_call('cscript {path}prnmngr.vbs -t -p "{nom}"'.format(nom=nom, path=scriptpath))
	except:
		logging.error("Impossible de définir {nom} comme imprimante par défaut".format(nom=nom))

def lister_ptr():
	""" Liste les imprimantes installées
	"""
	printers = []
	for a in win32print.EnumPrinters(2):
		# a(flags, description, name, comment)
		printers.append(a[2])
	return(printers)

def purge_jobs(imp):
	""" Purge la liste d'attente des jobs d'impression
	"""
	pattern = re.compile("ID de travail (\d+)")
	# Lister les jobs
	logging.info("Listage des travaux de {nom}".format(nom=imp))
	try:
		liste = subprocess.check_output('cscript {path}prnjobs.vbs -l'.format(path=scriptpath))
	except:
		logging.error("Impossible de lister les travaux")
		return False
	# Purger chaque job
	logging.info("Suppression des jobs en attente sur {nom}".format(nom=imp, path=scriptpath))
	for l in pattern.finditer(liste):
		i = l.group(1)
		logging.debug('cscript {path}prnjobs.vbs -x -p "{nom}" -j {idj}'.format(path=scriptpath, nom=imp, idj=i))
		try:
			subprocess.check_call('cscript {path}prnjobs.vbs -x -p "{nom}" -j {idj}'.format(path=scriptpath, nom=imp, idj=i))
		except:
			logging.error("Impossible de supprimer le job {idj}".format(idj=i))
	
#------------------------
#####  Préparation  #####
# Lecture des paramètres
parser = argparse.ArgumentParser(prog='gerer_les_imprimantes')
parser.add_argument('--defauts', action='store_true', help='Uniquement définir les imprimantes par défaut')
parser.add_argument('--purge', action='store_true', help='Supprime toutes les imprimantes avant réinstallation')
parser.add_argument('--est64bits', action='store_true', help='Si le système est 64 bits (win7)')
parser.add_argument('workingdir', nargs=1, help='Dossier contenant les .cfg et les .vbs (si besoin)')
args = parser.parse_args(sys.argv[1:])
est64bits = args.est64bits

# Les chemins d'accès des fichiers
wd = args.workingdir.pop() + os.sep
if est64bits:
	scriptpath = 'C:\\Windows\\SysWOW64\\Printing_Admin_Scripts\\fr-FR\\'
else:
	scriptpath = wd

# Fichier de log
fichier_log=r'C:\imprimantes.log'
logging.basicConfig(
	filename=fichier_log,
	level=logging.DEBUG,
	format='%(asctime)s - %(levelname)s: %(message)s')

logging.debug("## Version 64 bits ? {0} ##".format(est64bits))
# Chargement de la liste des imprimantes
config = ConfigParser.ConfigParser()
logging.debug("wd: {g}".format(g=wd))
config.read(wd + 'config.cfg')
imprimantes = {}
for sec in config.sections():
	if sec != "General":
		imprimantes[sec] = {}
		for key, val in config.items(sec):
			imprimantes[sec][key] = val
# Options générales
chemin_inf=config.get('General', 'chemin_inf')
nettoyage=config.get('General', 'nettoyage')
if args.purge:
	# Nettoyage des imprimantes forcé par paramètre
	nettoyage = "oui"
ignorer_ptr=config.get('General', 'ignorer_ptr')
# Chargement de la liste des modèles et de leur fichier INF
mod = ConfigParser.ConfigParser()
mod.read(wd + 'modeles.cfg')
modeles = {}
for sec in mod.sections():
	modeles[sec] = {}
	for key, val in mod.items(sec):
		modeles[sec][key] = val
# Récupération des infos concernant la machine
ip = socket.gethostbyname(socket.gethostname())
(hote, groupe) = nslook(ip)
logging.debug("Infos sur la machine : hote:{hote} / groupe:{groupe}".format(hote=hote, groupe=groupe))
# En cas de pb d'identification du poste -> abandon
if hote == '' or groupe == '':
	exit(1)

#----------------------------------
#####  Supressions et ajouts  #####
printers = lister_ptr()
logging.debug("Liste des imprimantes : {ptr}".format(ptr=', '.join(printers)))
# On regarde les imprimantes installées et on retire celles qui sont déclarées mais non affectées
if not args.defauts:
	for ptr in printers:
		#Nettoyage des files d'attente
		purge_jobs(ptr)
		if nettoyage == "oui": # Purge !
			supprimer_ptr(ptr)
		elif ptr in imprimantes:
			aff = imprimantes[ptr]['affectations']
			if groupe not in aff and hote not in aff:
				supprimer_ptr(ptr)

printers = lister_ptr()
# On ajoute les imprimantes affectées non encore installées
for imp in imprimantes:
	if not 'affectations' in imprimantes[imp]:
		logging.debug("{nom} n'a pas d'affectations".format(nom=imp))
		continue
	affectations = imprimantes[imp]['affectations']
	index = -42
	if groupe in affectations:
		index = string.find(affectations, groupe)
	elif hote in affectations:
		index = string.find(affectations, hote)
	if index >= 0:
		if imp not in printers and not args.defauts:
			ajouter_ptr(imp)
		if affectations[index-1] == "*":
			default_ptr(imp)
logging.debug("------  Installation des imprimantes terminée  ------")
